#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "http_server.h"

#define REQUEST_MAX_SIZE 5000
#define RESPONSE_MIN_SIZE 2000

#define LOG_VERBOSE(msg) {printf("HTTP_SERVER: %s\n", msg);}
#define LOG_ERROR(msg) {fprintf(stderr, "HTTP_SERVER error: %s\n", msg);}
#define LOG_FATAL(msg) {fprintf(stderr, "HTTP_SERVER fatal: %s\n", msg); exit(EXIT_FAILURE);}
#define MALLOC_OR_FATAL(var, size) {var = malloc(size); if(var == NULL){ LOG_FATAL("Could not allocate memory!");}}

static int socket_srv;

static void *clientHandler(void*);
static http_server_request_t * initClientRequest();
static void freeClientRequest(http_server_request_t*);
static http_server_response_t * initClientResponse();
static void freeClientResponse(http_server_response_t *r);
static void WriteResponseCode(int,char*);

/**
 * Structure used to pass information to client thread
 */
struct client_info {
	int socket;
	http_server_t *server;
	char *name;
};

/**
 * Information about a server route
 */
struct route_info {
	char *uri;
	route_callback cb;
	struct route_info *next_route;
};

/**
 * server information
 */
struct http_server_t {
	uint port;
	char *name;
	struct route_info *last_route;
};


/**
 * Utilities
 */
//Got to the end of a string
static char * strend(char * str){
	return strchr(str, '\0');
}

//Copy the content of a string to a newly allocated string
static char * CopyStrToNewVar(const char * const str){
	char * dest = NULL;
	MALLOC_OR_FATAL(dest, sizeof(char)*(strlen(str) + 1));
	strcpy(dest, str);
	return dest;
}


/**
 * Routes management
 */
struct route_info * init_route(){
	struct route_info *route;
	MALLOC_OR_FATAL(route, sizeof(struct route_info));
	route->uri = NULL;
	return route;
}

void free_route(struct route_info *route){
	if(route->uri != NULL)
		free(route->uri);

	free(route);
}

// Clear a server routte
void free_server_routes(struct route_info *last){
	struct route_info *n;
	while(last != NULL){
		n = last->next_route;
		free_route(last);
		last = n;
	}
}

//Find a route for a given URI
struct route_info * find_server_route(struct route_info *last,
	const char const * uri) {

	while(last != NULL) {

		if(strcmp(uri, last->uri) == 0)
			return last;

		last = last->next_route;
	}

	//the route was not found
	return NULL;
}



http_server_t * http_server_init () {

	http_server_t *server;
	MALLOC_OR_FATAL(server, sizeof(http_server_t));
	server->name = NULL;
	server->last_route = NULL;
	return server;

}


void http_server_quit(http_server_t * server) {


	if(server->name != NULL)
		free(server->name);

	if(server->last_route != NULL)
		free_server_routes(server->last_route);

	free(server);
}


void http_server_add_route(http_server_t * server,
	const char const *uri, route_callback cb) {

	struct route_info *route = init_route();
	route->uri = CopyStrToNewVar(uri);
	route->cb = cb;
	route->next_route = server->last_route;
	server->last_route = route;

}

static struct route_info * http_server_find_route(http_server_t * server, 
	const char const *uri) {
	return find_server_route(server->last_route, uri);
}



void http_server_serve(http_server_t * server, 
	int port,
	const char * const serverName) {

	//Get server information
	server->port = port;
	MALLOC_OR_FATAL(server->name, strlen(serverName) + 1)
	strcpy(server->name, serverName);

	socket_srv = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_srv == -1)
		LOG_FATAL("Could not create server socket!");

	//Bind socket to port
	struct sockaddr_in server_sock;
	server_sock.sin_family = AF_INET;
	server_sock.sin_addr.s_addr = INADDR_ANY;
	server_sock.sin_port = htons(server->port);
	if(bind(socket_srv, (struct sockaddr *)&server_sock, sizeof(server_sock)) < 0)
		LOG_FATAL("Could not bind server socket!");

	//Listen to socket
	listen(socket_srv, 5);


	//Accept all incoming connections (no protection)
	int new_socket = 0;
	struct sockaddr_in client;
	int c = sizeof(struct sockaddr_in);
	while( (new_socket = accept(socket_srv, (struct sockaddr *)&client, (socklen_t*)&c)) ) {

		//Create new handler thread for the socket
		struct client_info *client_socket_info_ptr;
		MALLOC_OR_FATAL(client_socket_info_ptr, sizeof(struct client_info));
		client_socket_info_ptr->socket = new_socket;
		client_socket_info_ptr->server = server;
		client_socket_info_ptr->name = inet_ntoa(client.sin_addr);

		pthread_t client_thread;
		if(pthread_create(&client_thread, NULL, clientHandler, (void*)client_socket_info_ptr) < 0)
			LOG_ERROR("Could not start a thread to handle client connection!");
		pthread_detach(client_thread);
	}

}


#define CLOSE_CLIENT_REQUEST {freeClientRequest(req); freeClientResponse(response); close(socket); return NULL;}
#define CHECK_NOT_NULL(var){if(var == NULL) {LOG_ERROR("Invalid client request!"); CLOSE_CLIENT_REQUEST;}}


// Extract information of an header, return NULL in case of failure
static char * getHeader(const char const * name, const char const * request){

	if(strlen(name) > 50)
		LOG_FATAL("Header name too long!");

	char header_name[50];
	sprintf(header_name, "\n%s: ", name);

	char *begin = strstr(request, header_name);
	if(begin == NULL) return NULL;
	begin += strlen(header_name);
	char *end = strchr(begin, '\n') - 1;
	if(end == NULL) return NULL;

	char *header = NULL;
	MALLOC_OR_FATAL(header, sizeof(char)*(end - begin + 1));
	strncpy(header, begin, end - begin);
	*(header + (end - begin)) = '\0';


	return header;

}

//Handle a client connection
static void *clientHandler(void * data) {

	struct client_info *info = (struct client_info*)data;
	int socket = info->socket;
	http_server_t *server = info->server;
	char * client_name = info->name;
	free(data);

	//Get server request
	char request[REQUEST_MAX_SIZE];
	read(socket, request, REQUEST_MAX_SIZE);


	//Process request
	http_server_request_t *req = initClientRequest();
	req->client_address = client_name;

	//Prepare response
	http_server_response_t *response = initClientResponse();


	//Extract URI
	char *begin_uri = strchr(request, ' ') + 1;
	CHECK_NOT_NULL(begin_uri);
	char *end_uri = strchr(begin_uri+1, ' ');
	CHECK_NOT_NULL(end_uri);
	if(end_uri - begin_uri > 255){
		LOG_ERROR("Too big client request!");
		CLOSE_CLIENT_REQUEST;
	}
	MALLOC_OR_FATAL(req->uri, end_uri - begin_uri + 1);
	strncpy(req->uri, begin_uri, end_uri - begin_uri);
	*(req->uri + (end_uri - begin_uri)) = '\0';

	req->host = getHeader("Host", request);
	req->user_agent = getHeader("User-Agent", request);

	//Process routes
	struct route_info *route = http_server_find_route(server, req->uri);
	if(route != NULL)
		route->cb(req, response);

	//Default content if required
	if(response->content == NULL){
		char message[50];
		sprintf(message, "Response code: %u", response->code);
		response->content = CopyStrToNewVar(message);
	}

	//Default content type
	if(response->content_type == NULL)
		response->content_type = CopyStrToNewVar("text/html");
	
	//Send response to client
	char *response_str;
	MALLOC_OR_FATAL(response_str, RESPONSE_MIN_SIZE);

	//Essential headers
	sprintf(response_str, "HTTP/1.1 %u ", response->code);
	WriteResponseCode(response->code, strchr(response_str, '\0'));
	sprintf(strend(response_str), "\n");
	sprintf(strend(response_str), "Server: %s\n", server->name);
	sprintf(strend(response_str), "Content-Type: %s\n", response->content_type);
	sprintf(strend(response_str), "Content-Length: %d\n", strlen(response->content));
	write(socket, response_str, strlen(response_str));

	//Additionnal headers
	if(response->headers != NULL)
		write(socket, response->headers, strlen(response->headers));

	write(socket, "\n", 1);
	write(socket, response->content, strlen(response->content) + 1);

	//Log request
	char message[500];
	sprintf(message, 
		"%s\t%s\t%s\t%d\t%s", 
		(req->host == NULL ? " " : req->host), 
		req->client_address,
		req->uri, 
		response->code,
		(req->user_agent == NULL ? "" : req->user_agent)
	);
	LOG_VERBOSE(message);

	free(response_str);
	CLOSE_CLIENT_REQUEST;
}

static http_server_request_t * initClientRequest(){
	http_server_request_t *req = NULL;
	MALLOC_OR_FATAL(req, sizeof(http_server_request_t));
	req->uri = NULL;
	req->host = NULL;
	req->client_address = NULL;
	req->user_agent = NULL;
	return req;
}

static void freeClientRequest(http_server_request_t *r){
	if(r->uri != NULL) free(r->uri);
	if(r->host != NULL) free(r->host);
	if(r->user_agent != NULL) free(r->user_agent);
	free(r);
}

static http_server_response_t * initClientResponse() {
	http_server_response_t *response = NULL;
	MALLOC_OR_FATAL(response, sizeof(http_server_response_t));
	response->code = 404;
	response->content = NULL;
	response->headers = NULL;
	response->content_type = NULL;
	return response;
}

static void freeClientResponse(http_server_response_t *r){
	if(r->content != NULL) free(r->content);
	if(r->content_type != NULL) free(r->content_type);
	if(r->headers != NULL) free(r->headers);
	free(r);
}

//Append the HTTP status to the response (ex OK - Bad Request)
static void WriteResponseCode(int code, char *dest){
	switch(code){

		case 200:
			strcpy(dest, "OK");
			break;

		case 404:
			strcpy(dest, "Not Found");
			break;

		case 401:
			strcpy(dest, "Bad Request");
			break;

		default:
			strcpy(dest, "Code");
	}
}