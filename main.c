/**
 * Simple HTTP Server test script
 *
 * @author Pierre HUBERT
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "http_server.h"

#define SERVER_PORT 6770
#define SERVER_NAME "My server"

void route_index(const http_server_request_t *request, 
	http_server_response_t *response) {

	response->code = 200;

	char * message = "<a href='/second'>Hello world</a>";
	response->content = malloc(sizeof(char)*(strlen(message) + 1));
	strcpy(response->content, message);

}

void route_two(const http_server_request_t *request, 
	http_server_response_t *response) {

	response->code = 200;

	char * message = "<h1>Second page</h1><input type='button' onclick='alert(\"Hello world\");' value='Click me' />";
	response->content = malloc(sizeof(char)*(strlen(message) + 1));
	strcpy(response->content, message);

}


int main(int argc, char **argv) {

	http_server_t *server = http_server_init();

	http_server_add_route(server, "/", &route_index);
	http_server_add_route(server, "/second", &route_two);

	printf("Listen on port %d\n", SERVER_PORT);
	http_server_serve(server, SERVER_PORT, SERVER_NAME);
	http_server_quit(server);

	return EXIT_SUCCESS;
}