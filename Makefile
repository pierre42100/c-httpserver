CC=gcc
CFLAGS= -Wall -pedantic -lpthread -ggdb3
LIBS=
DEPS=
OBJ= main.o http_server.o

all: http_server

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

http_server: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -rf *.o *.a *.exe http_server

run: all
	./http_server

debug:
	valgrind --leak-check=full --show-leak-kinds=all ./http_server